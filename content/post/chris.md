---
title: "Sonnenblume wachse!"
date: 2020-04-07T13:52:12+02:00
draft: false
tags: ["CindyJS","E-I-S","sunflowers"]
featured_image: '/images/chris-sunflower.jpg'
---
# Die Sonnenblumenkerne sind winkelabhängig golden

{{< cindyjs name="sunflowers" height="100%">}}
{{< download file="sunflowers.cdy" title="sunflowers.cdy">}}
