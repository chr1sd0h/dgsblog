---
title: "Teilerspielereien"
date: 2020-04-07T13:52:12+02:00
draft: false
tags: ["CindyJS","E-I-S"]
featured_image: '/images/sebastian-teiler.jpg'
---

# Bunte Teiler

An dieser Funktionen kann man die Teler einer Zahl ablesen.
Diese werden dabei in Farbe hervorgehoben.

{{< cindyjs name="CooleTeiler" height="50%">}}
