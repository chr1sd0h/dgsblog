---
title: "Sinus und Kosinus am Einheitskreis"
date: 2020-04-07T13:57:12+02:00
draft: false
tags: ["CindyJS"]
featured_image: '/images/steve-winkel.jpg'
---

# Eine Veranschaulichung der Winkelbeziehungen

Im Applet wird die Verbindung zwischen dem Einheitskreis und den Funktionen Sinus und Kosinus dargestellt.

{{< cindyjs name="Winkelbeziehungen" height="75%" >}}
{{< download file="Winkelbeziehungen.cdy" title="Winkelbeziehungen.cdy">}}
