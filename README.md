# DGS »advanced fun« Blog

Ein Gemeinschaftsprojekt aller Studierenden des Kurses im Wintersemester 2019/20.

[zum Blog...](https://chr1sd0h.gitlab.io/dgsblog/)

![DGSBlog-2020](https://chr1sd0h.gitlab.io/dgsblog/images/DGSBlog-2020.jpg)


# Creative Commons
[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/deed.de
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg